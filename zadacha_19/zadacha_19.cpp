﻿#include <stdlib.h>
#include <stdio.h>

typedef struct Node {
	int num;
	Node* next;
	Node* prev;
};

typedef struct List {
	Node* head;
	Node* current;
};

// Функция добавления элемента в список
void AddToList(List* list, Node** new_elem) {
	// Если списка не существует - выводим ошибку и завершаем работу
	if (list == NULL) {
		printf("Ошибка!");
		return;
	}
	else {
		// Проверяем головной элемент
		// Если его нет - значит мы добавляем голову
		if (list->head == NULL) {
			list->head = *new_elem;
			//(*new_elem)->next = *new_elem;
		}
		// Если он есть - значит добавляем новый
		else {
			// Начинаем цикл с "головы"
			list->current = list->head;
			// Цикл пока не доходим до конца списка, т.е. "головы"
			while (list->current->next) {
				list->current = list->current->next;
			};

			// Задаем новый элемент - следующим
			list->current->next = *new_elem;
			// И предыдущим
			(*new_elem)->prev = list->current;
		}
	}
}

Node* ReadElement() {
	Node* new_elem = (Node*)calloc(1, sizeof(Node));
	new_elem->next = NULL;

	int c = 0;
	scanf("%i", &c);

	if (c == 0)
		return NULL;
	else
		new_elem->num = c;

	return new_elem;
}

List* ReadList() {
	// Выделяем память на первый элемент
	List* list = (List*)calloc(1, sizeof(List));

	Node* new_element;
	printf("Ввод производится построчно.\n");
	printf("Для прекращения ввода введите 0...\n");
	while (new_element = ReadElement()) {
		AddToList(list, &new_element);
	}

	return list;
}

void AddEmptyNodeAfter(List *list, Node* after, int num = 0)
{
	Node* tmp = after->next;
	Node* newn = (Node*)calloc(1, sizeof(Node));
	newn->num = num;

	after->next = newn;
	newn->next = tmp;
	newn->prev = after;
	if (tmp)
		tmp->prev = newn;
}

void WriteElement(Node* current) {
	FILE* f = fopen("result.txt", "a");

	fprintf(f, "%i\n", current->num);

	fclose(f);
}

void WriteList(List* list) {
	list->current = list->head;

	while (list->current) {
		WriteElement(list->current);
		list->current = list->current->next;
	}
}

bool ProcessString(Node* current) {
	return current->num & (1 << 8 * sizeof(int) - 1);
}

void ProcessList(List* list) {
	list->current = list->head;

	do {
		if (ProcessString(list->current))
			AddEmptyNodeAfter(list, list->current);
		list->current = list->current->next;
	} while (list->current);
}

int main() {
	// Предварительно создаем/очищаем файл
	FILE* f = fopen("result.txt", "w");
	fclose(f);

	// Считываем данные из консоли
	List* list = ReadList();
	// Обрабатываем список
	ProcessList(list);
	// Выводим список
	WriteList(list);

	list->current = list->head;
	do {
		Node* tmp = list->current;
		list->current = list->current->next;
		free(tmp);
	} while (list->current);
	list = NULL;

	getchar();

	return 0;
}
