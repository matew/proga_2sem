﻿#include <stdlib.h>
#include <stdio.h>

typedef struct Node {
	char* text;
	Node* next;
	Node* prev;
};

typedef struct List {
	Node* head;
	Node* current;
};

// Функция добавления элемента в список
void AddToList(List* list, Node** new_elem) {
	// Если списка не существует - выводим ошибку и завершаем работу
	if (list == NULL) {
		printf("Ошибка!");
		return;
	}
	else {
		// Проверяем головной элемент
		// Если его нет - значит мы добавляем голову
		if (list->head == NULL) {
			list->head = *new_elem;
		}
		// Если он есть - значит добавляем новый
		else {
			// Начинаем цикл с "головы"
			list->current = list->head;
			// Цикл пока не доходим до конца списка
			do {
				list->current = list->current->next;
			} while (list->current->next);

			// Задаем новый элемент - следующим
			list->current->next = *new_elem;
			// И предыдущим
			(*new_elem)->prev = list->current;
		}
	}
}

Node* ReadElement() {
	Node* new_elem = (Node*)malloc(sizeof(Node));
	new_elem->next = NULL;

	char* c = NULL;

	char temp;
	int i = 1;
	while (scanf("%c", &temp) && temp != '\n' && temp != 0) {
		if (temp != '\n') {
			c = (char*)realloc(c, i * sizeof(char));
			c[i - 1] = temp;
			c[i] = '\0';
			i++;
		}
	}

	if (c[0] != '0')
		new_elem->text = c;
	else return NULL;

	return new_elem;
}

List* ReadList() {
	// Выделяем память на первый элемент
	List* list = (List*)calloc(1, sizeof(List));

	Node* new_element;
	printf("Ввод производится построчно.\n");
	printf("Для прекращения ввода введите 0...\n");
	while (new_element = ReadElement()) {
		AddToList(list, &new_element);
	}

	return list;
}

void DeleteNode(List* list, Node* node) {
	Node* tmp = NULL;

	if (node == list->head)
	{
		tmp = list->head->next;
		free(list->head);
		list->head = tmp;
		if (tmp)
			tmp->prev = NULL;
		node = NULL;
	}
	else
	{
		tmp = node->prev;
			tmp->next = node->next;
		node->next->prev = tmp;
		free(node);
	}
}

void WriteElement(Node* current) {
	FILE* f = fopen("result.txt", "a");

	for (int i = 0; current->text[i] != '\0'; i++) {
		fprintf(f, "%c", current->text[i]);
	}
	fprintf(f, "\n");

	fclose(f);
}

void WriteList(List* list) {
	list->current = list->head;

	do {
		WriteElement(list->current);
		list->current = list->current->next;
	} while (list->current != list->head);
}

bool ProcessString(Node* current) {
	int n = 0, res = 0;
	printf("Введите номер бита, который нужно проверить в строке: ");
	scanf("%d", &n);

	for (int i = 0; i < current->text[i] != '\0' && res < 2; i++) {
		if (current->text[i] & (1 << n))
			res++;
	}

	// Встретили два символа, где заданный бит = "1"
	if (res == 2)
		return true;
	else
		return false;
}

void ProcessList(List* list) {
	list->current = list->head;

	do {
		if (ProcessString(list->current))
			DeleteNode(list, list->current);
		list->current = list->current->next;
	} while (list->current);
}

int main() {
	// Предварительно создаем/очищаем файл
	FILE* f = fopen("result.txt", "w");
	fclose(f);

	// Считываем данные из консоли
	List* list = ReadList();
	// Обрабатываем список
	ProcessList(list);

	getchar();

	return 0;
}
