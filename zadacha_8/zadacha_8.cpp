﻿#include <stdlib.h>
#include <stdio.h>

typedef struct Node {
	char* text;
	Node* next;
};

typedef struct List {
	Node* head;
	Node* current;
};

// Функция добавления элемента в список
void AddToList(List* list, Node** new_elem) {
	// Если списка не существует - выводим ошибку и завершаем работу
	if (list == NULL) {
		printf("Ошибка!");
		return;
	}
	else {
		// Проверяем головной элемент
		// Если его нет - значит мы добавляем голову
		if (list->head == NULL) {
			list->head = *new_elem;
			(*new_elem)->next = *new_elem;
		}
		// Если он есть - значит добавляем новый
		else {
			// Начинаем цикл с "головы"
			list->current = list->head;
			// Цикл пока не доходим до конца списка, т.е. "головы"
			do {
				list->current = list->current->next;
			} while (list->current->next);

			// Задаем новый элемент - следующим
			list->current->next = *new_elem;
		}
	}
}

Node* ReadElement() {
	Node* new_elem = (Node*)malloc(sizeof(Node));
	new_elem->next = NULL;

	char* c = NULL;

	char temp;
	int i = 1;
	while (scanf("%c", &temp) && temp != '\n' && temp != 0) {
		if (temp != '\n') {
			c = (char*)realloc(c, i * sizeof(char));
			c[i - 1] = temp;
			c[i] = '\0';
			i++;
		}
	}

	if (c[0] != '0')
		new_elem->text = c;
	else return NULL;

	return new_elem;
}

List* ReadList() {
	// Выделяем память на первый элемент
	List* list = (List*)calloc(1, sizeof(List));

	Node* new_element;
	printf("Ввод производится построчно.\n");
	printf("Для прекращения ввода введите 0...\n");
	while (new_element = ReadElement()) {
		AddToList(list, &new_element);
	}

	return list;
}

void WriteElement(Node* current) {
	FILE* f = fopen("result.txt", "a");

	for (int i = 0; current->text[i] != '\0'; i++) {
		fprintf(f, "%c", current->text[i]);
	}
	fprintf(f, "\n");

	fclose(f);
}

bool ProcessString(Node* current) {	
	bool parity = false;

	for (int i = 0; current->text[i] != '\0' && !parity; i++) {
		int res = 0;
		for (int j = 8 * sizeof(char) - 1; j >= 0; j--) {
			// Если в 2-й записи "1" - увеличиваем счётчик
			if (current->text[i] & (1 << j))
				++res;
		}
		if (res % 2 == 0)
			parity = true;
	}

	return parity;
}

void ProcessList(List* list) {
	list->current = list->head;

	do {
		if (ProcessString(list->current))
			WriteElement(list->current);
		list->current = list->current->next;
	} while (list->current != list->head);
}

int main() {
	// Предварительно создаем/очищаем файл
	FILE* f = fopen("result.txt", "w");
	fclose(f);

	// Считываем данные из консоли
	List* list = ReadList();
	// Обрабатываем список
	ProcessList(list);

	getchar();

	return 0;
}
