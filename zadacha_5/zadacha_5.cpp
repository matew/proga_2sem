﻿#include <stdlib.h>
#include <stdio.h>

typedef struct Node {
	char symbol;
	Node* next;
	Node* prev;
};

typedef struct List {
	Node* head;
	Node* current;
};

// Функция добавления элемента в список
void AddToList(List* list, Node** new_elem) {
	// Если списка не существует - выводим ошибку и завершаем работу
	if (list == NULL) {
		printf("Ошибка!");
		return;
	}
	else {
		// Проверяем головной элемент
		// Если его нет - значит мы добавляем голову
		if (list->head == NULL) {
			list->head = *new_elem;
			(*new_elem)->next = *new_elem;
			(*new_elem)->prev = *new_elem;
		}
		// Если он есть - значит добавляем новый
		else {
			// Начинаем цикл с "головы"
			list->current = list->head;
			// Цикл пока не доходим до конца списка, т.е. "головы"
			do {
				list->current = list->current->next;
			} while (list->current->next != list->head);

			// Задаем новый элемент - следующим
			list->current->next = *new_elem;
			// Указатель на следующий элемент в новом 
			// элементе теперь указывает на "голову" списка
			(*new_elem)->next = list->head;
			// То же самое с предыдущим элементом
			list->head->prev = (*new_elem);
			(*new_elem)->prev = list->current;
		}
	}
}

Node* ReadElement() {
	Node* new_elem = (Node*)malloc(sizeof(Node));
	new_elem->next = NULL;

	char c = ' ';

	scanf("%c", &c);

	if (c != '\n')
		new_elem->symbol = c;
	else return NULL;

	return new_elem;
}

List* ReadList() {
	// Выделяем память на первый элемент
	List* list = (List*)calloc(1, sizeof(List));

	Node* new_element;
	printf("Ввод производится подряд идущими символами.\n");
	printf("Для прекращения ввода нажмите Enter...\n");
	while (new_element = ReadElement()) {
		AddToList(list, &new_element);
	}

	return list;
}

void WriteElement(Node* current) {
	FILE* f = fopen("result.txt", "a");

	fprintf(f, "%c", current->symbol);

	fclose(f);
}

void WriteList(List* list) {
	list->current = list->head;

	do {
		WriteElement(list->current);
		list->current = list->current->next;
	} while (list->current != list->head);
}

void ProcessNode(Node* current) {
	int res = 0;

	for (int i = 8 * sizeof(char) - 1; i >= 0; i--) {
		// Если в 2-й записи "0" - увеличиваем счётчик
		if (!(current->symbol & (1 << i)))
			++res;
	}

	if (res % 2 == 0)
		WriteElement(current);
}

void ProcessList(List* list) {
	list->current = list->head;

	do {
		ProcessNode(list->current);
		list->current = list->current->next;
	} while (list->current != list->head);
}

int main() {
	// Предварительно создаем/очищаем файл
	FILE* f = fopen("result.txt", "w");
	fclose(f);

	// Считываем данные из консоли
	List* list = ReadList();
	// Обрабатываем список
	ProcessList(list);

	getchar();

	return 0;
}
